<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::any("upload", function (Request $request){
    $request->validate([
        "file" => "required|file"
    ]);

    if ($file = $request->file("file")) {
        $path = storage_path("app/public/uploads");
        $name = \Illuminate\Support\Str::random(7).time();
        if ($file->move($path, $name)) {
            return response()->json([
                "status" => "success",
                "message" => "File uploaded successfully",
                "code" => "00",
            ], 200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "File upload failed",
                "code" => "01",
            ], 400);
        }

    }
});

Route::resource('articles', 'ArticleAPIController');
